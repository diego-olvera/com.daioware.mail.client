package com.daioWare.mail.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.daioware.mail.client.MailUser;

public class MailProperties {
	
	private static MailUser sender;
	
	static{
		Properties properties=new Properties();
		try {
			properties.load(new FileInputStream(new File("src/test/resources/mail.properties")));
			sender=new MailUser(properties.getProperty("mail.username"),
					properties.getProperty("mail.password"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static MailUser getSender() {
		return sender;
	}	
}
