package com.daioWare.mail.client;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.daioware.mail.client.MailBody;
import com.daioware.mail.client.MailEvent;
import com.daioware.mail.client.MailMessenger;
import com.daioware.mail.client.MailReceiver;
import com.daioware.mail.client.MailUser;
import com.daioware.mail.client.SendMailException;

public class MailTest {
	protected static 
	DateTimeFormatter iCalendarDateFormat=DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'");

	@Test
	public void sendTest() throws SendMailException{
		MailUser user=MailProperties.getSender();
		String subject="Calendar test á";
		MailBody body=new MailBody("hello");
		List<MailReceiver> receivers=new ArrayList<>();
		receivers.add(new MailReceiver("diego6569olvera@gmail.com"));
		MailMessenger messenger=new MailMessenger(subject, body, receivers);
		LocalDateTime start=LocalDateTime.now().plusDays(1);
		
		LocalDateTime end=LocalDateTime.now().plusDays(1).plusMinutes(30);
		MailEvent mailEvent=new MailEvent(1,"Title","Somewhere","Some description",start,end
				,user);
		messenger.setMailEvent(mailEvent);
		mailEvent.addParticipant(new MailUser("diego6569@hotmail.com"));
		messenger.setDefaultUser(user);
		System.out.println("start:"+start.atZone(ZoneId.systemDefault()));
		System.out.println("format start:"+start.format(iCalendarDateFormat));
		System.out.println("end:"+end.atZone(ZoneId.systemDefault()));
		System.out.println("format end:"+end.format(iCalendarDateFormat));
		System.out.println("Sending...");
		try {
			messenger.send();
			System.out.println("Sent");
			assertTrue(true);
		} catch (SendMailException e) {
			e.printStackTrace();
			throw e;
		}
	}
}
