package com.daioware.mail.client;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

import com.daioware.dateUtilities.schedule.Event;

public class MailEvent extends Event implements Iterable<MailUser>{

	private MailUser organizer;
	private MailUser mailTo;
	private int id;
	private ArrayList<MailUser> participants=new ArrayList<>();
	
	public MailEvent(int id,String title, String where, String description, LocalDateTime startDate, LocalDateTime endDate
			,MailUser organizer,MailUser mailTo) {
		super(title, where, description, startDate, endDate);
		setOrganizer(organizer);
		setMailTo(mailTo);
	}
	public MailEvent(int id,String title, String where, String description, LocalDateTime startDate, LocalDateTime endDate
			,MailUser organizer) {
		this(id,title, where, description, startDate, endDate,organizer,organizer);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public MailUser getOrganizer() {
		return organizer;
	}
	public void setOrganizer(MailUser organizer) {
		this.organizer = organizer;
	}
	public MailUser getMailTo() {
		return mailTo;
	}
	public void setMailTo(MailUser mailTo) {
		this.mailTo = mailTo;
	}
	public boolean addParticipant(MailUser r){
		if(participants.contains(r)){
			return false;
		}
		return participants.add(r);
	}
	public boolean removeParticipant(MailUser r){
		return participants.remove(r);
	}
	public int participantsSize(){
		return participants.size();
	}
	public MailUser getParticipant(int p){
		return participants.get(p);
	}
	@Override
	public Iterator<MailUser> iterator() {
		return participants.iterator();
	}

}
